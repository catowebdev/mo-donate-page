#!/usr/bin/perl

use CGI qw(:standard);

print header("text/plain");
print "The passed parameters were:\n";
for my $key ( param() ) {
  print "$key => ", param($key), "\n";
}

