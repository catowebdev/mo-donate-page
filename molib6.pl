#!/usr/bin/perl -w

use strict;

use utf8;
use Carp;
use LWP::UserAgent;
use LWP::Debug qw(+conn);
use HTTP::Request;
use HTTP::Response;
use XML::LibXML;
#use XML::LibXML::Reader;
#use XML::Simple;
#use Data::Dumper;
use MoXAddPerson;
use MoXAddGift;

my ($personID, $prefix, $lname, $fname, $middleinit, $email, $address, $address2, $city, $state, $zip, $country, $phone, $billing_address, $billing_address2, $billing_city, $billing_state, $billing_zip, $billing_country, $ccnumber, $expmonth, $expyear, $expdate, $cvv, $recurring, $cardname, $total_amount, $AddPerson, $AddPersonResult, $AddGift, $AddGiftResult, $AddPersonparsexmlresult, $AddGiftparsexmlresult);

#$fname = "virginia";
#$lname = "anderson";
#$middleinit = "";
#$email = "vanderson\@cato.org";

$fname = "Kevin";
$lname = "Davis";
$middleinit = "";
#$email = "kevinsdesk\@aol.com";
$email = "vanderson\@cato.org";

#$fname = "michael";
#$lname = "stein";
#$email = 'michael@membersonlysoftware.com';

$prefix = "Mr.";
$address = "1000 Massachusetts Ave., NW";
$address2 = "";
$city = "Washington";
$state = "DC";
$zip = "20001";
$country = "0";
$phone = "2027895262";
$recurring = "false";
$ccnumber = "4003000123456781";
$expmonth = "12";
$expyear = "15";
$expdate = $expyear . $expmonth;
$total_amount = "16";
$cardname = "Kevin Davis";
$cvv = "697";
#$personID = "00108754";

    $AddPerson = new MoXAddPerson (
	 		prefix 		=>	$prefix,
	 		lastname 	=>	$lname,
	 		firstname 	=>	$fname,
	 		middleinit 	=>	$middleinit,
	 		address 	=>	$address,
	 		address2 	=>	$address2,
	 		city	 	=>	$city,
	 		state	 	=>	$state,
	 		zip	 	=>	$zip,
	 		country 	=>	$country,
			phone	 	=>	$phone,
			email	 	=>	$email

    );
$AddPersonResult = $AddPerson -> MoXAddPerson;

$AddPersonparsexmlresult = &parseXML("AddPerson", $AddPersonResult);

if ($AddPersonparsexmlresult =~ /^xout=(.*)/) {
  $personID = $1;
}

print "AddPersonResult = $AddPersonparsexmlresult\n";

#$AddGift = &MoXAddGift($personID, $ccnumber, $expdate, $recurring, $cardname, $total_amount, $email);

    $AddGift = new MoXAddGift (

                        personID       =>      $personID,
                        recurring      =>      $recurring,
                        ccnumber       =>      $ccnumber,
                        cardname       =>      $cardname,
                        expdate        =>      $expdate,
                        cvv            =>      $cvv,
                        totalamount    =>      $total_amount,
                        email          =>      $email

    );

$AddGiftResult = $AddGift -> MoXAddGift;

$AddGiftparsexmlresult = &parseXML("AddGift", $AddGiftResult);

print "AddGiftResult = $AddGiftparsexmlresult\n";
print "HERE I AM";

exit;
########################################################################
#Subroutines
########################################################################

sub moXFindPersonByEmail {

  my ($fn, $ln, $em, $soap_header, $soap_string, $soap_footer, $soap_message);

  ($fn, $ln, $em) = @_;

  $soap_header = qq(<soap:Envelope xmlns:ns="urn:nevrona.com/indysoap/v1/" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <soap:Body>
   <ns:moXFindPersonByEmail soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">);

  $soap_string = qq(
    <xIn xsi:type="xs:string">
      &#60;param&#62;
        &#60;FName&#62;$fn &#60;/FName&#62;
        &#60;LName&#62;$ln &#60;/LName&#62;
        &#60;Email&#62;$em &#60;/Email&#62;
      &#60;/param&#62;
    </xIn>);

  $soap_footer = qq(
<ErrorMsg xsi:type="xs:string"></ErrorMsg>
<xOut xsi:type="xs:string"></xOut>
</ns:moXFindPersonByEmail>
</soap:Body>
</soap:Envelope>);

  $soap_message = $soap_header . $soap_string . $soap_footer;
  
  return $soap_message;

}

########################################################################
## Parse MO API output
########################################################################

sub parseXML {

  my ($caller, $resp, $parser, $doc, $node, $fault, $return, $xout, $error, $PersonID, $ourreturn);

  ($caller, $resp) = @_;
  
  $resp = $resp -> as_string;
  print "XML == $resp\n\n";

  $resp =~ s/^.*?</</s; #remove HTTP response headers

  $parser = new XML::LibXML();

  $doc = $parser->load_xml(string => $resp);
  print "doc = $doc\n";

  for $node ($doc -> findnodes('//*')) {
    if ($node -> nodeName =~ /soap:Fault/) {
        $fault = "$node";
        #print "fault = $fault\n";
    }
    else {
        if ($node -> nodeName =~ /^return/) {
            $return = $node -> textContent;
            #print "return = $return\n";
        }
        if ($node -> nodeName =~ /^xOut/) {
            $xout = $node -> textContent;
            if ($caller eq "AddGift") {
                if (($xout =~ /<\s*AuthCode\s*>(\d*)<\/Auth/) || ($xout =~ /&#60;AuthCode&#62;(\d*)&#60;\/Auth/)) {
                    $xout = $1;
                }
                else {
                    $xout = "123";
                }
            }
            else {
              return "xout=$xout";
            }
        #print "xout = $xout\n";
        }
        if ($node -> nodeName =~ /^ErrorMsg/) {
            $error = $node -> textContent;
            #print "error = $error\n";
        }
    }
  }

  if ($fault) {
    return "fault=$fault";
  }
  else {
    if (($return eq "true") && ($xout)) {
      #print "return = xout $xout\n";
      $ourreturn = "xout=$xout";
    }
    elsif (($return eq "false") || ($return eq "error")) {
      #print "return = error $error\n";
      $ourreturn = "error=$error";
    }
    else {
      $ourreturn = "";
    }
  }
  #print "OURRETURN = $ourreturn\n";
  return $ourreturn;
}




