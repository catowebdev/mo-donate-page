#!/usr/bin/perl -w

use strict;
use warnings;
use DBD::SQLite;
use DateTime;
use DateTime::Format::SQLite;
#use DateTime::Duration;
my ($badip, $database, $dbh, $sth, $sth_array_ref, $last_access, $plus15, $minus15, $id, @banned, $banned_now, $plus15f);

$badip = "0.0.0.5"; #my test IP, should be users IP, passed to function

$database = "/www/securessl/cgi-bin/scripts/db/snoopy.db";

## connect to the database
eval {
  $dbh = DBI->connect("dbi:SQLite:dbname=$database", "", "",  { RaiseError => 1 }) or die $DBI::errstr;

  if ($dbh) {
    # print "module connected to DB ok\n";
    ## Insert IP into table, sqlite assigns key and timestamp.
    $dbh->do("insert into snoopy ( ip ) values ( \"$badip\" )") or die $DBI::errstr;
 
    ## Now snoop on this IP -- is it banned for now, banned forever, or does it need to be? grab
    ## all instances of the IP, latest first, save to an array
    $sth = $dbh->prepare("select * from snoopy where ip=\"$badip\" order by id desc") or die $sth->errstr;
    $sth->execute() or die $sth->errstr;
    $sth_array_ref = $dbh->selectall_arrayref($sth) or die $sth->errstr;

    #first array element will be latest access; save key; grab timestamp; add and subtract 15 minutes
    $id = $sth_array_ref->[0]->[0];
    #print "id = $id\n";
    $last_access = $sth_array_ref->[0]->[2];
    #print "last access = $last_access\n";
    $plus15 = (DateTime::Format::SQLite->parse_datetime($last_access)->add( minutes => 15));
    $minus15 = (DateTime::Format::SQLite->parse_datetime($last_access)->add( minutes => -15));
    #print "last access plus 15 = $plus15, last access minus 15 = $minus15\n";

    #initialize counters
    my $i = 0;
    my $j = 0;
    my $k = 0;

    for (@{$sth_array_ref})  { #loop through everything in the array, get some counts
      #print "select rtns id = $_->[0], ip = $_->[1], last accessed = $_->[2],banned_until = $_->[3], banned_forever = $_->[4]\n";
      if (defined($_->[4]) > 0) { #have we been banned forever
        $i++;
      }
      if (defined($_->[3])) { #have we been banned_until
        $j++;
        push (@banned, $_->[3]);
      }
      if (DateTime::Format::SQLite->parse_datetime($_->[2]) > $minus15) { #accesses over last 15 minutes
        $k++;
      }
    }
    #print "i = $i, j = $j, k = $k\n";
 
    #done counting, let's see what the numbers are and what we need to do
    if ($i > 0) { #we have been banned forever, bail here  
      print "banned forever, bail here\n";
    }
    elsif ($j >= 5) { #we've been banned more than 5 times, time to ban forever
      print "it's time to ban forever, insert 1 & exit\n";
      $dbh -> do("update snoopy set banned_forever = '1' where id = \"$id\"") or die $dbh->errstr;
      print "banning forever bail here\n";
    } 
    elsif (($j < 5) && ($j > 0)) { 
      #print "we have experienced banning, let's see if we're currently banned\n";
      $banned_now = shift(@banned); #get first banned, should be most recent
      if (DateTime::Format::SQLite->parse_datetime($banned_now) > DateTime::Format::SQLite->parse_datetime($last_access)) {
        print "we've been banned until $banned_now, exit here\n";
      }
      else { #let's see if we need banning
        if ($k > 5) { #time to ban for now
          #print "k is greater than 5. it's time to ban for now, format plus15, update record, & exit\n";
          $plus15f = DateTime::Format::SQLite->format_datetime($plus15);
          $dbh -> do("update snoopy set banned_until = \"$plus15f\" where id = \"$id\"") or die $dbh->errstr;
          print "we've been banned until $banned_now, exit here\n";
        }
      }
    }
    else {
      print "everything ok\n";
    }

  } #end dbh test

  $dbh -> disconnect;
}; #end eval

if ($@) {
  print "error: $@\n";
}

exit;
