create table errors (
id integer primary key,
date timestamp not null default current_timestamp,
prefix char,
lastname varchar,
firstname varchar,
zip varchar,
phone varchar,
email varchar,
remote_address varchar,
msg varchar
);

