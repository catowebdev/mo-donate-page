#!/usr/bin/perl

use v5.14;
#use strict;
use diagnostics;
use utf8;
#use Net::SMTP;
use Net::SMTP_auth;


my @errors = "This is a test error message";

&email_admin(@errors);


sub send_email {
  my ($smtp, $emailfrom, $msg, @to, @bads, @ok);
  ($emailfrom, $msg, @to) = @_;
  print "passed from and to: $emailfrom ... @to\n";

  #$smtp = Net::SMTP_auth->new('exchange.cato.org', Hello => 'exchange.cato.org');
  $smtp = Net::SMTP_auth->new('smtp.networkalliance.net', Debug => 1);
  #$smtp = Net::SMTP_auth->new('smtp.networkalliance.net');

  if ($smtp) {
    print "SMTP connection made\n";
    eval {
      $smtp->auth('LOGIN', 'friedrich', 'cato!984');
      print "Auth ok\n";
      $smtp -> mail($emailfrom);
      print "emailfrom ok\n";
      @ok = $smtp -> recipient(@to,{SkipBad=>1}) or return $smtp->message;
      print "ok = @ok\n";
      unless (@ok == @to) {
        @bads = &cmparray(@ok, @to);
        foreach (@bads) {
          unless ($_ =~ "") {
            &log_error("can't send mail to: $_");
          }
        }
      }
      $smtp -> data($msg);
      $smtp -> quit;
      print "done mailing\n";
    };
    if ($@) {
      print "we have an error message: $@\n";
      #&log_error("error in email eval: $@");
      #&email_admin($@, $emailfrom, $msg, @to);
      #return 0;
    }
    else {
      #return 1;
      print "I'm returning 1\n";
    }
  }
  else {
    print "Could not connect to email server, could not send email: $!";
    #return 0;
  }
}

#####################################################################
sub cmparray {

  my (@array1, @array2, @union, @intersection, @difference, %count, $element);
  @array1 = shift;
  @array2 = @_;

  @union = @intersection = @difference = ();
  %count = ();

  foreach $element (@array1, @array2) { $count{$element}++;}

  foreach $element (keys %count) {
    push @union, $element;
    push @{ $count{$element} > 1 ? \@intersection : \@difference }, $element;
  }

  return @difference;
}

######################################################################
## Send email to admin if errors 
######################################################################

sub email_admin {

  my ($from, $admin, @to, $msg1, $error_msg, $referer, $prefix, $firstname, $lastname, $email, $badip);
  print "MADE IT TO EMAILADMIN";
  $error_msg = @_;
  print "error msg = $error_msg<br />\n";
  $referer = "me";
  $admin = "vanderson\@cato.org";
  @to = ($admin);
  $from = "webserver\@cato.org";

  $msg1 = <<"EOF";
From: $from
To: $admin
Subject: Sponsorship ERROR

Error message: $error_msg

Sponsor's info:
Salutation\t\t=\t$prefix

First name\t\t=\t$firstname

Last name\t\t=\t$lastname

Email\t\t\t=\t$email

IP\t\t\t=\t$badip

Referer\t\t=\t$referer

EOF

  &send_email($from, $msg1, @to);
}

