#!/usr/bin/perl

use Date::Calc qw( Date_to_Time Time_to_Date);


$time = "2013-11-30 17:47:01";
@date1 = ($time =~ /(\d{4})-(\d{2})-(\d{2})\s(\d{2}):(\d{2}):(\d{2})/);
print "$time, @date1\n";
foreach (@date1) {
  print "$_ \n";
}
@date2 = (2013,11,30,20,18,01);

#  @date1 = (2002,8,31,23,59,1);
#  @date2 = (2002,9,1,11,30,59); # ==> less than 12 hours
#
#  #@date1 = (2002,8,31,22,59,1);
#  #@date2 = (2002,9,1,11,30,59); # ==> more than 12 hours
#
$d1 = Date_to_Time(@date1);
$d2 = Date_to_Time(@date2);
#
if ($d1 <= $d2) { print "The two dates are in chronological order.\n"; }
else            { print "The two dates are in reversed order.\n"; }
#
$d1a = $d1 - 1*15*60; #less 5 minutes
if ($d1a <= $d2) { print "date1 less 12 hours, date2 is More.\n"; }
else                       { print "Less than 12 hours.\n"; }
print "d1a = $d1a, d2 = $d2\n";
@date1a = Time_to_Date($d1a);
@date2 = Time_to_Date($d2);
print "d1 = @date1a, d2 = @date2\n";

