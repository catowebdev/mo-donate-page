#!/usr/bin/perl -w

use strict;
use warnings;
use diagnostics;
use DBI;
use DBD::SQLite;
use Date::Calc qw( Date_to_Time Time_to_Date );

my ($badip, $database, $dbh, $sth1, $sth1_array_ref, $last_access, $sth2, $sth2_array_ref, $sth3, $sth3_array_ref, $count_attempts, $count_bans, $last_acc_dt, $banned_until_result);

$database = "/www/securessl/cgi-bin/scripts/db/snoopy.db";
 
$badip = "0.0.0.5"; 

eval {
  $dbh = DBI->connect("dbi:SQLite:dbname=$database", "", "", { RaiseError => 1 }) or die "$DBI::errstr, sqlite3_errmsg(_dbHandle)";
  if ($dbh) {
    print "Connected to DB ok<br />\n";
    ## Insert IP into table and auto timestamp.
    $dbh -> do("INSERT INTO snoopy ( ip ) VALUES ('$badip')") or die $DBI::errstr;

    ## Grab the timestamp from this attempt and all other attempts
    $sth1 = $dbh->prepare("SELECT * FROM snoopy WHERE ip='$badip' ORDER BY datetimenow DESC");
    $sth1_array_ref  = $dbh->selectall_arrayref($sth1) or die DBI::errstr;;
    $sth1->finish();

    if (@{$sth1_array_ref}) { #query returned results, get time for this attempt
      $last_access = $sth1_array_ref->[0]->[2];
      print "last access = $last_access\n";

      #let's see if this ip has been banned for now or forever or needs to be
      foreach (@{$sth1_array_ref}) { 
      #loop through array ref -- if ${$aref}[3] is too hard to read, you can write $aref->[3] instead
      #look for anything in banned forever
        print "sth1 returns ip = $_->[1], last accessed =  $_->[2], banned_until = $_->[3], banned_forever = $_->[4]\n";
        if (defined($_->[4])) { #banned_forever is not null, we're outie
          print "banned forever, drop out of this loop, adios!\n";
          #return 1;
        }
      }
      foreach (@{$sth1_array_ref}) { #not banned forever, banned for now?
        if (defined($_->[3])) { #there's a value in banned_until, let's see if it's relevant to this attempt
          $banned_until_result = &time($last_access, $_->[3], "1*15*16"); #are we within 15 minutes
          if ($banned_until_result) {
            print "we're still banned, drop out\n";
          }
          else {
            print "continue testing...n";
          }
        }
      }       
      #now let's see if we need to make updates 
      $count_bans = 0;
      $count_attempts = 0;

      foreach (@{$sth2_array_ref}) { #loop through our select rows again
        if (&time($last_access, $_->[3], "12*60*60")) { #count how many times we were banned in the last 12 hours
          print "banned within last 12 hours.\n"; 
          $count_bans++;
        }
      }
      if ($count_bans > 3) { #we've been banned at least 3 times in the last 12 hours, this is bad behavior
        print "we are banned forever, make update statement here\n";
      }
      else { #not banned more than 3 times, let's see if we need to ban for 15 minutes
        foreach (@$sth2_array_ref) { #loop through our select rows again
          if (&time($last_access, $_->[2], "1*15*60")) { #offset 15 minutes
            $count_attempts++;
            print "accessed $count_attempts times within last 1 hour.\n"; 
          }
        }
        if ($count_attempts > 3) { 
          print "we should be banned for now\n"; #update db
        }
      }
    }    
    else { #empty array returned
      print "nothing in sth2 array ref\n";
    }
  } #end initial if dbh
  $dbh -> disconnect;
}; #end eval
if ($@) {
  print "error: $@\n";
}


####################################################################

sub time {
  #$date1 is $last_access in this instance; $date2 is last_access over the offset time
  my ($date1, $date2, $offset) = @_;
  my (@date1, @date2, $d1, $d2);

  #$time = "2013-11-30 17:47:01";
  #format for calc, s/b last access -- same as now -- we are comparing to
  @date1 = ($date1 =~ /(\d{4})-(\d{2})-(\d{2})\s(\d{2}):(\d{2}):(\d{2})/);
  #print "@date1\n";

  #@date2 = (2013,11,30,20,18,01);
  @date2 = ($date2 =~ /(\d{4})-(\d{2})-(\d{2})\s(\d{2}):(\d{2}):(\d{2})/);

#  @date1 = (2002,8,31,23,59,1);
#  @date2 = (2002,9,1,11,30,59); # ==> less than 12 hours
#
#  #@date1 = (2002,8,31,22,59,1);
#  #@date2 = (2002,9,1,11,30,59); # ==> more than 12 hours
#
  $d1 = Date_to_Time(@date1);
  $d2 = Date_to_Time(@date2);
  if ($offset > 0) {
    if (($d1 - $offset) <= $d2) { 
      print "date2 is more than date1 less offset.\n"; 
      return 1;
    }
    else { 
      print "date2 is less than d1 minus offset, not in range.\n"; 
      return 0;
    }
  }
  else { 
    if ($d1 <= $d2) { 
      print "date2 is more than date1 no offset.\n"; 
      return 1;
    }
    else { 
      print "date2 is less than d1 no offset.\n"; 
      return 0;
    }
  }

#@date1a = Time_to_Date($d1a);
#@date2 = Time_to_Date($d2);
#print "d1 = @date1a, d2 = @date2\n";
}
