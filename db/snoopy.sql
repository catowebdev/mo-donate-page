create table snoopy (
id integer primary key,
ip varchar not null,
datetimenow timestamp not null default current_timestamp,
banned_until timestamp,
banned_forever integer
);

