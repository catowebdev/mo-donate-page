create table donate (
id integer primary key not null,
date timestamp not null default current_timestamp,
personID int,
prefix char,
lastname varchar,
firstname varchar,
addrtype int,
company varchar ,
street varchar ,
city varchar ,
state char,
zip varchar,
country int,
phone varchar,
email varchar,
recurring int,
donation_amount int,
GLaccount varchar,
auth_code int,
remote_address varchar
);

