#!/usr/bin/perl -w

use strict;
use warnings;
use DBD::SQLite;


my ($badip, $database, $dbh, $sth1, $sth1_array_ref, $last_access, $sth2, $sth2_array_ref);

$badip = "0.0.0.4";

$database = "/www/securessl/cgi-bin/scripts/db/snoopy.db";

## connect to the database
eval {
  $dbh = DBI->connect("dbi:SQLite:dbname=$database", "", "",  { RaiseError => 1, AutoCommit => 0 }) or die $DBI::errstr;

  if ($dbh) {
    print "module connected to DB ok\n";
    ## Insert IP into table with automatic timestamp.
    $dbh -> do("insert into snoopy ( ip ) values (\"$badip\")") or die $dbh->errstr;

    ## Grab the timestamp from this attempt
    $sth1 = $dbh->prepare("select datetimenow from snoopy where ip='$badip' order by datetimenow desc limit 1");
    $sth1_array_ref = $dbh->selectrow_arrayref($sth1) or die $sth1->errstr;
    $last_access = $sth1_array_ref->[0];
    print "last access = $last_access\n";
    $sth1->finish();

    ## Now see if this IP has been banned
    print "at sth2, badip = $badip, last_access = $last_access\n";
    $sth2 = $dbh->prepare("select * from snoopy where ip=\"$badip\" and banned_until > datetime(\"$last_access\")");
    #$sth2 = $dbh->prepare("select * from snoopy where ip=\"$badip\" ") or die $sth2->errstr;
    $sth2_array_ref = $dbh->selectall_arrayref($sth2) or die $sth2->errstr;
    # If there's anything returned by this query, then this IP is banned
    if ($sth2_array_ref) {
      foreach (@$sth2_array_ref) {
        print "sth2  select returns ip = $_->[1], last accessed = $_->[2], banned_until = $_->[3]\n";
      }
    } 
    else {
      print "nothing in sth2 array ref\n";
    }
  }
  ## nothing in sth2, is it time to ban this ip?





  $dbh -> disconnect;
};
if ($@) {
  print "error: $@\n";
}


