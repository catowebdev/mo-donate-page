# old subroutine to send notification to donor after successful submit. Now handled by MO
#####################################################################
#################################################################

sub autorespond {

  my ($from, @to, $name, $cc_nice, $msg1, $xaccount);
  $from = "$emailto";
  @to = ($email);

  if ($prefix) {
    $name = $prefix . " " . $lastname;
  }
  else {
    $name = $firstname . " " . $lastname;
  }

  $cc_nice = &cc_nice($creditcard);

  $xaccount = &xaccount;

  if ($renewal eq "Capital Campaign") {
    $msg1 = <<"EOF";
From: $from
To: $email
Subject: Confirmation of Cato Institute Capital Campaign Contribution

Dear $name,

Thank you very much for your generous contribution to Cato Institute's 
capital campaign, "Liberating the Future."  

EOF
  }
  else {
    $msg1 = <<"EOF";
From: $from
To: $email
Subject: Confirmation of Cato Institute Contribution

Dear $name, 

Thank you for your generous contribution to the Cato Institute.\n
EOF
  }

  if ($sponsor_level) {
    $msg1 .= "Sponsorship level\t=\t$sponsor_level\n";
}

$msg1 .= <<"EOF";
Donation amount\t\t=\t\$$donation_amount\n
Payment method\t\t=\t$cc_nice\n
Account No.\t\t=\t$xaccount\n

Name\t\t\t=\t$prefix $firstname $lastname\n
Address\t\t\t=\t$street\n
\t\t\t\t$city, $state $zip\n 
Country\t\t\t=\t$country\n
Phone\t\t\t=\t$phone (phone)\n
Email address\t\t=\t$email\n\n
EOF


  if ($renewal eq "Capital Campaign") {
    $msg1 .= <<"EOF";

Shortly, you will receive a formal acknowledgement of your 
contribution in the mail.  We will keep you and other contributors 
to the capital campaign updated on our progress.  Thanks again for 
this generous gift. 

Cordially, 
 

Lesley Albanese
Cato Institute
1000 Massachusetts Ave NW
Washington, DC 20001
202-789-5223 
lalbanese\@cato.org  

The Cato Institute is a nonprofit, tax-exempt educational foundation 
under Section 501(c)3 of the Internal Revenue Code.  Contributions 
are received from individuals, foundations, corporations, and partnerships 
and are tax-deductible to the extent allowable by law.

EOF
  }
  else {
$msg1 .= <<"EOF";

Shortly, you will receive a formal acknowledgement of your 
contribution in the mail. In the meantime, we look forward 
to sharing all of Cato's efforts on behalf of individual 
liberty and free markets with you over the coming year. 

Cordially, 
 

Lesley Albanese
Cato Institute
1000 Massachusetts Ave NW
Washington, DC 20001
202-789-5223 
lalbanese\@cato.org  

P.S.  As a Cato Sponsor, you will receive 35% off of purchases of 
Cato books and publications at our store http://www.catostore.org/.  
Please use the code Sponsors to receive your discount. 

The Cato Institute is a nonprofit, tax-exempt educational foundation 
under Section 501(c)3 of the Internal Revenue Code.  Contributions 
are received from individuals, foundations, corporations, and partnerships 
and are tax-deductible to the extent allowable by law.

EOF
}

  &send_email($from, $msg1, @to);
}


