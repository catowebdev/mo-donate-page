#!/usr/bin/perl -w

use strict;

use 5.010;
use utf8;
use Carp;
use LWP::UserAgent;
use LWP::Debug qw(+conn);
use HTTP::Request;
use HTTP::Response;
use XML::LibXML;
#use Unicode::String;
#use Encode;
#use Search::Tools::UTF8;
#use File::BOM qw( :all );

my ($lname, $fname, $email, $message, $useragent, $request, $response, $parser, $doc, $root, @children, $child, $response_string, @nodes, $node, $return, $xOut, $ErrorMsg, $output, $method, $xout, $error);

#$fname = "virginia";
#$lname = "anderson";
#$email = "vanderson\@cato.org";

$fname = "michael";
$lname = "stein";
$email = 'michael@membersonlysoftware.com';

$message = "<soap:Envelope xmlns:ns=\"urn:nevrona.com/indysoap/v1/\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
   <soap:Body>
   <ns:moXFindPersonByEmail soap:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">
<xIn xsi:type=\"xs:string\">&#60;param&#62;&#60;FName&#62;$fname &#60;/FName&#62;&#60;LName&#62;$lname &#60;/LName&#62;&#60;Email&#62;$email &#60;/Email&#62;&#60;/param&#62;</xIn>
<ErrorMsg xsi:type=\"xs:string\"></ErrorMsg>
<xOut xsi:type=\"xs:string\"></xOut>
</ns:moXFindPersonByEmail>
</soap:Body>
</soap:Envelope>";
print "$message\n";
$useragent = LWP::UserAgent->new();
$request = HTTP::Request->new(POST => 'http://hayek.membersonlysoftware.com:1027/soap/');
$request->content_type("text/xml; charset=utf-16");
$request->content($message);
$response = $useragent->request($request);
print $response->as_string;
$response = $response -> as_string;
#$response = decode_from_bom($response);


#$response = encode('utf8', $response);

#$response = Encode::decode('utf8', $response);

# my $str = $response;
 
# print "bad UTF-8 sequence: " . find_bad_utf8($str)
#    unless is_valid_utf8($str);
 
# print "bad ascii byte at position " . find_bad_ascii($str)
#    unless is_ascii($str);
 
# print "bad latin1 byte at position " . find_bad_latin1($str)
#    unless is_latin1($str);


#$response = Encode::is_utf16($response);
#print "1 $response\n";

#$response = Unicode::String::utf8($response);



# A restrictive list, which # should be modified to match # an appropriate RFC, for example.
#my $OK_CHARS='-a-zA-Z0-9_.@><?\=/":\s\''; 


#$response =~ s/[^$OK_CHARS]//igo;
$response =~ s/^.*?</</s; #remove HTTP response headers
#$response =~ s/UTF-16/UTF-8/;
    
$parser = new XML::LibXML();

$doc = $parser->load_xml(string => $response);
print "doc = $doc\n";

for $node ($doc->findnodes('//*')){
    if ($node -> nodeName =~ /^return/) {
        print "found node return\n";
        $return = $node -> textContent;
    }
    if ($node -> nodeName =~ /^xOut/) {
        print "found xout\n";
        $xout = $node -> textContent;
        print "xout = $xout\n";
    }
    if ($node -> nodeName =~ /^ErrorMsg/) {
        print "found error\n";
        $error = $node -> textContent;
        print "error = $error\n";
    }
}
print "error = $error, return = $return, xout = $xout\n";

if ($return eq "true") {
    print "$xout\n";
}
else {
    print "$error\n";
}


exit;

 
