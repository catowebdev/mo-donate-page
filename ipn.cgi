#!/usr/bin/perl

# It is highly recommended that you use version 6 upwards of 
# the UserAgent module since it provides for tighter server 
# certificate validation

use LWP::UserAgent 6;
use DBD::SQLite;

#$|++;

read (STDIN, $query, $ENV{'CONTENT_LENGTH'});
$query .= '&cmd=_notify-validate';


# send http 200 ok empty header back to paypal
# send post back to PayPal system to validate -- includes header???

$ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 1 });
$req = HTTP::Request->new('POST', 'https://www.sandbox.paypal.com/cgi-bin/webscr');
$req->content_type('application/x-www-form-urlencoded');
$req->header(Host => 'www.sandbox.paypal.com');
$req->content($query);
$res = $ua->request($req);

# split posted variables into pairs

@pairs = split(/&/, $query);

$count = 0;

foreach $pair (@pairs) { 
  ($name, $value) = split(/=/, $pair);
  $value =~ tr/+/ /;
  $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
  $variable{$name} = $value;
  $count++;
}

# assign posted variables to local variables for testing

$item_name = $variable{'item_name1'}; 
$item_number = $variable{'item_number1'}; 
$payment_status = $variable{'payment_status'};
$payment_amount = $variable{'mc_gross'}; 
$payment_currency = $variable{'mc_currency'};
$txn_id = $variable{'txn_id'};
$receiver_email = $variable{'receiver_email'};
$payer_email = $variable{'payer_email'};

if ($res->is_error) { 
 # log error, ipn.log
open ($fh, '>>./ipn.log');
print $fh `date` . "\n";
print $fh "res is error\n";
foreach my $key (keys %variable) {
  print $fh "key = $key, value = $variable{$key}<br />\n";
}
print $fh "\n\n";
close $fh;

}
elsif ($res->content eq 'VERIFIED') {
# check that $payment_status=Completed 
# check that $txn_id has not been previously processed 
  #open ipn.db, select id from ipn where txn_id = $txn_id
    #if 0 results, then it's a new txn
    #if >0 results, it's already been processed, ignore
# check that $receiver_email is your Primary PayPal email 
# check that $payment_amount/$payment_currency are correct 
# process payment
&insert_hash("ipn", %variable);

}
elsif ($res->content eq 'INVALID') { 
open ($fh, '>>./ipn.log');
print $fh `date` . "\n";
print $fh "res content = INVALID\n";
foreach my $key (keys %variable) {
  print $fh "key = $key, value = $variable{$key}<br />\n";
}
print $fh "\n\n";
close $fh;

# log for manual investigation, ipn.log

} 
else { 
  # log for manual investigation, something weird happened
}

print "content-type: text/plain\n\n";

sub insert_hash {
  my ($table, %vars) = @_;
  $database = "/www/securessl/cgi-bin/scripts/db/ipn.db";

  eval {
    $dbh = DBI->connect("dbi:SQLite:dbname=$database", "", "", { RaiseError => 1 }) or die "$DBI::errstr";
    if ($dbh) {
      $dbh->trace(2);
      #print "Connected to DB ipn ok<br />\n";

      # sort to keep field order, and thus sql, stable for prepare_cached
      my @keys = sort keys %vars;
      my @values = values %vars;
      my $sql = sprintf "insert into %s (%s) values (%s)",
        $table, join(",", @keys), join(",", ("?")x@keys);
      my $sth = $dbh->prepare_cached($sql);
      #my $sth = $dbh->prepare("INSERT INTO ipn (key, value) VALUES (?,?);");
      #$sth->execute_array({},\@keys, \@values) or die $DBI::errstr;
      return $sth->execute(@values) or die $DBI::errstr;
    }
  };
  if ($@) {
    # log error, ipn.log
    open ($fh, '>>./ipn.log');
    print $fh `date` . "\n";
    print $fh "db error = $@\n";
    foreach my $key (keys %variable) {
      print $fh "key = $key, value = $variable{$key}<br />\n";
    }
    print $fh "\n\n";
    close $fh;
  }
  undef $dbh;
}

# https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_notify-validate&mc_gross=19.95&protection_eligibility=Eligible&address_status=confirmed&payer_id=LPLWNMTBWMFAY&tax=0.00&...&payment_gross=19.95&shipping=0.00q

#my @keys = keys %hash;
#
#my @values = values %hash;
#
#my $sth = $dbh->prepare("INSERT INTO table1(id, value) VALUES (?,?);");
#
#$sth->execute_array({},\@keys, \@values);

#$verify_sign = $variable{'verify_sign'};
#$payer_id = $variable{'payer_id'};
#$residence_county = $variable{'residence_country'};
#$variable{'address_state'};
#$variable{'mc_handling'};
#$variable{'receiver_email'};
#$variable{'item_number1'};
#$variable{'address_status'};
#$variable{'payment_type'};
#$variable{'address_street'};
#$variable{'business'};
#$variable{'address_city'};
#$variable{'payment_status'};
#$variable{'mc_shipping1'};
#$variable{'cmd'};
#$variable{'test_ipn'};
#$variable{'txn_type'};
#$variable{'address_country'};
#$variable{'payment_date'};
#$variable{'mc_handling1'};
#$variable{'invoice'};
#$variable{'payer_status'};
#$variable{'mc_fee'};
#$variable{'address_zip'};
#$variable{'custom'};
#$variable{'txn_id'};
#$variable{'last_name'};
#$variable{'receiver_id'};
#$variable{'address_country_code'};
#$variable{'mc_shipping'};
#$variable{'payer_email'};
#$variable{'tax'};
#$variable{'address_name'};
#$variable{'notify_version'};
#$variable{'mc_gross'};
#$variable{'mc_gross1'};
#$variable{'item_name1'};
#$variable{'first_name'};
#$variable{'mc_currency'};
