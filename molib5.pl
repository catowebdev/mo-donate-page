#!/usr/bin/perl -w

use strict;

use utf8;
use Carp;
use LWP::UserAgent 6;
#use LWP::Protocol::https;
use LWP::Debug qw(+conn);
use HTTP::Request;
use HTTP::Response;
use XML::LibXML;
#use Mozilla::CA;
#use XML::LibXML::Reader;
#use XML::Simple;
#use Data::Dumper;
#use Unicode::String;
#use Encode;
#use Search::Tools::UTF8;
#use File::BOM qw( :all );

my ($personID, $prefix, $lname, $fname, $email, $address, $address2, $city, $state, $zip, $country, $phone, $billing_address, $billing_address2, $billing_city, $billing_state, $billing_zip, $billing_country, $ccnumber, $expmonth, $expyear, $expdate, $recurring, $cardname, $total_amount, $xmlmessage, $xmlresult, $parsexmlresult);

#
$fname = "Kevin";
$lname = "Davis";
$email = "vanderson\@cato.org";

#$fname = "michael";
#$lname = "stein";
#$email = 'michael@membersonlysoftware.com';

$prefix = "mr";
$address = "1010 Mass Ave.";
$address2 = "";
$city = "washington";
$state = "dc";
$zip = "20001";
$country = "us";
$billing_address = "1010 Mass Ave.";
$billing_address2 = "";
$billing_city = "washington";
$billing_state = "dc";
$billing_zip = "20001";
$billing_country = "us";
$phone = "7034776548";
$recurring = "false";
$ccnumber = "5499990123456781";
$expmonth = "12";
$expyear = "15";
$expdate = $expyear . $expmonth;
$total_amount = "12";
$cardname = "kevin davis";

#$xmlmessage = &moXFindPersonByEmail($fname, $lname, $email);

#$xmlresult = &sendXML($xmlmessage);

#$parsexmlresult = &parseXML($xmlresult);

#if ($parsexmlresult =~ /\d+/) {
#  $personID = $parsexmlresult;
#}
#else {
#  print "$parsexmlresult\n";
#}
#print "MoXAddPerson...\n";
#$xmlmessage = &moXAddPerson($personID, $prefix, $fname, $lname, $email, $address, $address2, $city, $state, $zip, $country, $billing_address, $billing_address2, $billing_city, $billing_state, $billing_zip, $billing_country, $phone);
#
#$xmlresult = &sendXML($xmlmessage);
#
#$parsexmlresult = &parseXML($xmlresult);

#print "$parsexmlresult\n";

print "MoXAddGift...\n";
$personID = "00302320";
$xmlmessage = &moXAddGift($personID, $ccnumber, $expdate, $recurring, $cardname, $total_amount, $email);

$xmlresult = &sendXML($xmlmessage);

$parsexmlresult = &parseXML($xmlresult);

print "$parsexmlresult\n";

exit;
########################################################################
#Subroutines
########################################################################

sub moXFindPersonByEmail {

  my ($fn, $ln, $em, $soap_header, $soap_string, $soap_footer, $soap_message);

  ($fn, $ln, $em) = @_;

  $soap_header = qq(<soap:Envelope xmlns:ns="urn:nevrona.com/indysoap/v1/" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <soap:Body>
   <ns:moXFindPersonByEmail soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">);

  $soap_string = qq(
    <xIn xsi:type="xs:string">
      &#60;param&#62;
        &#60;FName&#62;$fn &#60;/FName&#62;
        &#60;LName&#62;$ln &#60;/LName&#62;
        &#60;Email&#62;$em &#60;/Email&#62;
      &#60;/param&#62;
    </xIn>);

  $soap_footer = qq(
<ErrorMsg xsi:type="xs:string"></ErrorMsg>
<xOut xsi:type="xs:string"></xOut>
</ns:moXFindPersonByEmail>
</soap:Body>
</soap:Envelope>);

  $soap_message = $soap_header . $soap_string . $soap_footer;
  
  return $soap_message;

}

########################################################################

sub moXAddPerson {

  my ($pID, $pf, $fn, $ln, $em, $addr1, $addr2, $cty, $st, $z, $cntry, $ph, $baddr1, $baddr2, $bcty, $bst, $bz, $bcntry, $soap_header, $soap_string, $soap_footer, $soap_message);
  ($pID, $pf, $fn, $ln, $em, $addr1, $addr2, $cty, $st, $z, $cntry, $baddr1, $baddr2, $bcty, $bst, $bz, $bcntry, $ph) = @_;
  
  #$pID = "00108754";

  $soap_header = qq(<soap:Envelope xmlns:ns="urn:nevrona.com/indysoap/v1/" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <soap:Body>
   <ns:moXAddPerson soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><xIn xsi:type="xs:string">);

  $soap_string = qq(
      <PersonInfo Type="View">
      <Person Type="Entity">
          <ID>$pID</ID>
          <FName>$fn </FName>
          <LName>$ln </LName>
          <MName></MName>
          <Username></Username>
          <Password></Password>
          <IsOrgnMbr></IsOrgnMbr>       
          <Prefix>$pf</Prefix>                
          <Suffix></Suffix>
          <DoNotMail>False</DoNotMail>
          <DoNotEmail>False</DoNotEmail>
          <DoNotFax>True</DoNotFax>
          <StatusCode></StatusCode>        
          <Status></Status>                          
          <OrgnID></OrgnID>
          <SendConf>true</SendConf>
      </Person>
      <Address Type="Entity">
          <Action>1</Action>        
          <OwnerClassID>P</OwnerClassID>
          <OwnerID></OwnerID>
          <Refno></Refno>
          <Preferred>False</Preferred>
          <AddrTypeCode>2</AddrTypeCode>
          <Active>True</Active>
          <StartDate></StartDate>
          <EndDate></EndDate>
          <OrgnLine></OrgnLine>
          <Title></Title>
          <Street1>$address</Street1>
          <Street2>$address2</Street2>
          <Street3></Street3>
          <City>$city</City>
          <State>$state</State>
          <Zip>$zip</Zip>
          <CountryCode></CountryCode>
          <Country>$country</Country>
          <PLZ></PLZ>
      </Address>
      <Address Type="Entity">
          <Action>1</Action>        
          <OwnerClassID>P</OwnerClassID>
          <OwnerID></OwnerID>
          <Refno></Refno>
          <Preferred>False</Preferred>
          <AddrTypeCode>6</AddrTypeCode>
          <Active>True</Active>
          <StartDate></StartDate>
          <EndDate></EndDate>
          <OrgnLine></OrgnLine>
          <Title></Title>
          <Street1>$billing_address</Street1>
          <Street2>$billing_address2</Street2>
          <Street3></Street3>
          <City>$billing_city</City>
          <State>$billing_state</State>
          <Zip>$billing_zip</Zip>
          <CountryCode></CountryCode>
          <Country>$billing_country</Country>
          <PLZ></PLZ>
      </Address>
      <Phone Type="Entity">
          <Action>1</Action>
          <OwnerClassID>P</OwnerClassID>
          <OwnerID></OwnerID>
          <Refno></Refno>
          <Preferred></Preferred>
          <PhonetypeCode>0</PhonetypeCode>
          <Phonetype></Phonetype>
          <PhoneNumber>$ph</PhoneNumber>
          <Extn></Extn>
          <Comment></Comment>
      </Phone>
      <Phone Type="Entity">
          <Action>1</Action>
          <OwnerClassID>P</OwnerClassID>
          <OwnerID></OwnerID>
          <Refno></Refno>
          <Preferred></Preferred>
          <PhonetypeCode>0</PhonetypeCode>
          <Phonetype>Email</Phonetype>
          <PhoneNumber>$em</PhoneNumber>
          <Extn></Extn>
          <Comment></Comment>
      </Phone>
    </PersonInfo>);

# action: 0 = do nothing, 1 =  insert/save, 2 = delete
# &#60;FName&#62;$fn &#60;/FName&#62;
#
  $soap_footer = qq(
    </xIn>
    <ErrorMsg xsi:type="xs:string"></ErrorMsg>
    <xOut xsi:type="xs:string"></xOut>
    </ns:moXAddPerson>
    </soap:Body>
    </soap:Envelope>);

  $soap_string =~ s/</&#60;/g;
  $soap_string =~ s/>/&#62;/g;

  #print "$soap_string\n";

  $soap_message = $soap_header . $soap_string . $soap_footer;
  
  print "SOAP MESSAGE = $soap_message\n\n";

  return $soap_message;

}

########################################################################

sub parseXML {

  my ($resp, $parser, $doc, $node, $fault, $return, $xout, $error);

  $resp = shift;
  
  $resp = $resp -> as_string;
  print "XML == $resp\n\n";

  $resp =~ s/^.*?</</s; #remove HTTP response headers

  $parser = new XML::LibXML();

  $doc = $parser->load_xml(string => $resp);
  print "doc = $doc\n";

  for $node ($doc -> findnodes('//*')) {
    if ($node -> nodeName =~ /soap:Fault/) {
        $fault = "$node";
        #print "fault = $fault\n";
        last;
    }
    else {
        if ($node -> nodeName =~ /^return/) {
            $return = $node -> textContent;
            #print "return = $return\n";
        }
        if ($node -> nodeName =~ /^xOut/) {
            $xout = $node -> textContent;
            #print "xout = $xout\n";
        }
        if ($node -> nodeName =~ /^ErrorMsg/) {
            $error = $node -> textContent;
            #print "error = $error\n";
        }
    }
  }

  if (defined ($fault)) {
    return "fault=$fault";
  }
  else {
    if (defined ($return) && $return eq "true") {
      return "xout=$xout";
    }
    elsif (defined ($return) && $return eq "false") {
      return "error=$error";
    }
    else {
      return "undefined";
    }
  }
}

########################################################################

sub sendXML {

  my ($msg, $useragent, $request, $response);

  $msg = shift;
  
  $useragent = LWP::UserAgent->new();
  #$useragent->ssl_opts( verify_hostnames => 1 );
  #$useragent->ssl_opts( SSL_ca_file => Mozilla::CA::SSL_ca_file() );
  $useragent->ssl_opts( SSL_ca_file => '/etc/ssl/certs/MO.pem' );
  $request = HTTP::Request->new(POST => 'http://membersonly.cato.org:1599/soap/');
  #$request->header('If-SSL-Cert-Subject' => 'membersonly.cato.org');
  #$request = HTTP::Request->new(POST => 'https://208.82.213.178:1599/soap/');
  #208.82.213.178. the soap server is on port 1599 
  #$request = HTTP::Request->new(POST => 'http://hayek.membersonlysoftware.com:1027/soap/');
  $request->content_type("text/xml; charset=utf-16");
  $request->content($msg);
  $response = $useragent->request($request);

  #print "response = $response\n";
  return $response;

}

sub moXAddGift {

  my ($pID, $cvv, $ccnum, $expd, $recur, $ccname, $total, $em, $soap_header, $soap_string, $soap_footer, $soap_message);

  ($pID, $ccnum, $expd, $recur, $ccname, $total, $em) = @_;

  #$pID = "00302469";
  $pID = "00302320";
  $ccnum = "4003000123456781";
  $expd = "1512";
  $cvv = "123";
  $recur = "false";

  $soap_header = qq(<soap:Envelope xmlns:ns="urn:nevrona.com/indysoap/v1/" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <soap:Body>
   <ns:moXAddGift soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><xIn xsi:type="xs:string">);

  $soap_string = qq(
<GiftInfo>
   <ParentAPID>00471626</ParentAPID>
   <PayerID>$pID</PayerID>
   <IsAnonymous>false</IsAnonymous>
   <IsRecurring>$recur</IsRecurring>
   <IsCorpGift>false</IsCorpGift>
   <DedicationText></DedicationText>
   <Number>$ccnum</Number>
   <HolderName>$ccname</HolderName>
   <ExpDate>$expd</ExpDate>
   <CCID>$cvv</CCID>
   <AuthCode></AuthCode>
   <Amount>$total</Amount>
   <Email>$em</Email>
</GiftInfo>);
 
  $soap_footer = qq(
    </xIn>
    <xOut xsi:type="xs:string"></xOut>
    <ErrorMsg xsi:type="xs:string"></ErrorMsg>
    <return xsi:type="xs:boolean"></return>
    </ns:moXAddGift>
    </soap:Body>
    </soap:Envelope>);

  $soap_string =~ s/</&#60;/g;
  $soap_string =~ s/>/&#62;/g;

  #print "$soap_string\n";
 
  $soap_message = $soap_header . $soap_string . $soap_footer;
  print "$soap_message\n";
  
  return $soap_message;
 
}
