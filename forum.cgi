#!/usr/bin/perl -Tw
# 
# Form processing script. Emails form data to $recipient and sends page 
# back to user.

use strict;
use Net::SMTP;
use Net::SMTP_auth;
use Carp ();
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use CGI::SSI;
use Time::localtime;
use DBI;

#$ENV{'PATH'} = '/usr/lib';
#$ENV{'BASH_ENV'} = '/bin/bash';

$| = 1;

my ($q, $ssi, $logo, $event, $clean_event, $event_date, $confURL, $emailto, $location, $eventid, $special_invite, @emails, $emails, $responder, $thankyou, $prefix, $lastname, $firstname, $affiliation, $semester, $iyear, $title, $address, $address2, $city, $state, $zip, $country, $areacode, $phone, $email, $notes, @errors, $logme_status, $emailme_status);

$q = new CGI;
$ssi = new CGI::SSI;

# if it's a GET or other method instead of a POST redirect to home page
#if ($q -> request_method() !~ /POST/i) {
#  print $q -> redirect('http://www.cato.org/index.html');
#  exit;
#}

######################################################################
### MAIN
######################################################################

&start_thankyou;

&get_vars;

if (@errors) {
  &thankyou;
}
else {
  $emailme_status = &emailme;
  if ($emailme_status == 1) {
    &autorespond;
    &thankyou;
    &insert;
    &logme;
  }
  else { #unable to email conference staff
    print "Sorry, I am unable to email this registration to the conference staff. Please email webmaster\@cato.org and let us know you received this message. We apologize for any inconvenience.";
   &end_thankyou;
  }
}

exit;

######################################################################
### subroutines
######################################################################

######################################################################
### 
######################################################################

sub get_vars {

  $event = $q -> param('event');
  $clean_event = &replace_quotes($event);
  $logo = $q -> param('logo');
  $event_date = $q -> param('event_date');
  $confURL = $q -> param('confURL');
  $emailto = $q -> param('emailto');
  $location = $q -> param('location');
  $special_invite = $q -> param('special-invite');
  
  
  if (($emailto =~ /^(\w+)$/) || ($emailto =~ /^(\w+)\@cato\.org$/)) {
    $emailto = $1;
    $emailto .= "\@cato.org";
  }
  else {
    $emailto = "events\@cato.org";
  }

  if ($confURL =~ /^\/event\.php/) {
    $confURL = "http://www.cato.org" . $confURL;
  }
 
  $prefix = &clean($q -> param('prefix'), "Salutation");;
  $firstname = &clean($q -> param('firstname'), "First Name");
  $lastname = &clean($q -> param('lastname'), "Last Name");
  $affiliation = &clean($q -> param('affiliation'), "Affiliation");
  $title = &clean_unrequired($q -> param('title'), "Title");
  $address = &clean($q -> param('address'), "Address");
  $address2 = &clean_unrequired($q -> param('address_2'), "Address 2");
  $city = &clean($q -> param('city'), "City");
  $state = &clean($q -> param('state'), "State or Region");
  $zip = &clean($q -> param('zip'), "Zip or Postal Code");
  $country = &clean($q -> param('country'), "Country");
  $areacode = &clean($q -> param('areacode'), "Areacode");
  $phone = &clean($q -> param('phone'), "Phone Number");
  $email = &check_email($q -> param('email'), "Email Address");
 
#below lines are for intern alumni event only
 
  if ($event =~ /Intern Alumni Reunion/){
  $iyear = &clean($q -> param('iyear'), "Year");
  $semester = &clean($q -> param('session'), "Semester");
      }
  if ($semester) {
    $notes = $q -> param('other_attendees');
  }
#end intern alumni addition

  if ($title =~ /media/) {
    $notes = $q -> param('notes');
  }
}

######################################################################
### start browser page back to user
######################################################################

sub start_thankyou {

  print "Content-Type: text/html\n\n";
  print $ssi -> include(virtual => "http://www.cato.org/ssi/headers/doctype.htmlf");
  print "<html><head><title>Thank you for registering</title>\n";
  print $ssi -> include(virtual => "http://www.cato.org/ssi/headers/events-calendar-nonsmarty_cgi.htmlf");
  print $ssi -> include(virtual => "http://www.cato.org/ssi/headers/masthead_cgi.htmlf");
  print "<div id=\"leftnav\">";
  print $ssi -> include(virtual => "http://www.cato.org/ssi/nav/events-calendar.htmlf");
  print "</div>";
  print "<div id=\"centercol_wide\">";

}

######################################################################
### print page to user
######################################################################

sub thankyou {
my $time = time;
  if (@errors) {
    print "<br /><font color=\"red\"><p>Please check the following field(s) and make sure the information entered is correct.</p>";
    print "\n<ul>\n";
    foreach (@errors) {
      print "<li> $_</li><br /><br />\n";
    }
    print "</ul></font>\n";
    &print_form;
    &end_thankyou;
  }

  else {
    print <<EOF;
    <br />
    <h1>Cato Events</h1>
    <p>Thank you for your interest in 
    <strong>$event</strong>
    to be held $event_date, at the $location.</p>

<script type="text/javascript">

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-624231-15']);
_gaq.push(['_setDomainName', 'cato.org']);
_gaq.push(['_setAllowLinker', true]);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

</script>



    <script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-1677831-1");
; var pageTracker = _gat._getTracker("UA-1677831-1");
pageTracker._addTrans( "$time",
                       "$affiliation", 
                       "0.00",
                       "0.00", //tax
                       "0.00", //shipping
                       "$city",
                       "$state",
                       "$country"
);

pageTracker._addItem( "$time", // Order ID  --- same timestamp as above
                      "", // General ledger account
                      "free_registration", // Product  --- registration
                      "$event", // Category --- event name
                      "0.00", // Price --- 0.00 for free events
                      "1" // Quantity --- 1 for a simple event registration, but can change for actual paid events
);
pageTracker._trackTrans();
} catch(err) {}
</script>




EOF
  if ($location =~ /Cato/i) {
    print "<p>This message confirms your registration.</p>";
  }
  else {
    print "<p><!-- Please arrive early, as seating is limited.--> This message confirms your registration.</p>";
  }
    &end_thankyou;
  }


}

######################################################################
### start browser page back to user
######################################################################

sub end_thankyou {

   print <<EOF;
   <br /><br /><br />
   <p align="center">
   | <a href="$confURL">Event</a>
   | <a href="http://www.cato.org/events/calendar.html">Cato Events Calendar</a>
   | <a href="http://www.cato.org/index.html">Cato Institute Home</a>
   |
   </p>
EOF
    print "</div>"; 
    print $ssi -> include(virtual => "http://www.cato.org/ssi/footers/home_cgi.htmlf");
    print $q -> end_html;
}



######################################################################
### get date in "Month day, 0000" format
######################################################################

sub getTime_pretty {

  my ($year, %MONTHS, $month, $key, $day, $date_pretty); 

  $year = localtime->year();

  $year = $year - 100;
  if ($year < 10) {
    $year = "200$year";
  }
  else {
    $year = "20$year";
  }

  %MONTHS = ('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July',
 '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');

  $month = localtime->mon() + 1;

  if ($month < 10) {
    $month = "0$month";
  }

  foreach $key (sort keys %MONTHS) {
    if ($key eq $month) {
      $month = $MONTHS{$key};
    }
  }

  $day = localtime->mday();

  $date_pretty = $month . " " . $day . ", " . $year;

  return ($date_pretty);

}



######################################################################
### Email conference staff with registration info
#####################################################################

sub emailme {

  my ($from, $ccto, $ccto1, $ccto2, $msg1, $smtp, @to);

  $from = "webserver\@cato.org";
  $ccto1 = "alanp\@cato.org";
  #$ccto2 = "kmoody\@cato.org";
  #$ccto1 = "lhamo_la\@yahoo.com";
  #$ccto2 = "virginia.anderson\@gmail.com";
  #$emailto = "events\@cato.org";
  @to = ($emailto, $ccto1);

  $msg1 = <<"EOF";
From: $from
To: $emailto
CC: $ccto1 
Subject: Registration for $clean_event

  Event\t\t=\t$clean_event\n
  Date of event\t=\t$event_date\n 
  Name\t\t=\t$firstname $lastname\n
  Affiliation\t=\t$affiliation\n
  Title\t\t= \t$title\n
  \t\t\t=\t$semester\n
  \t\t\t=\t$iyear\n
  Address\t\t=\t$address\n
  \t\t\t=\t$address2\n
  \t\t\t=\t$city, $state $zip\n
  Country\t\t=\t$country\n 
  Phone/Fax\t\t=\t($areacode)-$phone\n
  Email\t\t=\t$email\n
EOF

  if ($notes) {
    $msg1 .= "  Notes\t\t=\t$notes\n";
  }

  if ($special_invite eq "yes") {
    $msg1 .= "  Special invite\t\t=\tyes\n";
  }


  my $status = &send_email($from, $msg1, @to);
  return $status;
}
######################################################################
### Trim leading and trailing spaces
#####################################################################

sub replace_quotes {

  my ($dirt) = shift;

  if ($dirt =~ /<\/?\w+\s*\/?>/) {
    $dirt =~ s/<\/?\w+\s*\/?>//g;
    return $dirt;
  }
  elsif ($dirt =~ /&amp\;&quot\;/) {
  #if ($dirt =~ /&amp\;&quot\;/) {
    $dirt =~ s/&amp\;&quot\;/"/g;
    return $dirt;
  }
  elsif ($dirt =~ /&quot\;/) {
    $dirt =~ s/&quot\;/"/g;
    return $dirt;
  }
  else {
    return $dirt;
  } 
}

#####################################################################
### Trim leading and trailing spaces
#####################################################################

sub clean {
  my (@subvars, $dirt, $field);
  @subvars = @_; 
  # print "SUBVARS CLEAN = index 0 = $subvars[0], index 1 = $subvars[1], index eq $#subvars<br />";
  if ($#subvars > 0) {
    $dirt = $subvars[0];
    $field = $subvars[1];
    $dirt =~ s/^\s+//;
    $dirt =~ s/\s+$//;
  }
  else {
    $field = $subvars[0];
    $dirt = "";
  }
  # print "dirt = $dirt";

  if ($dirt ne "") {
      if (
         ($dirt =~ /Content-Transfer-Encoding/i) ||
         ($dirt =~ /[bcc|cc]:\s+/i) ||
         ($dirt =~ /X-Mailer:/i) ||
         ($dirt =~ /^[\w.\-_]+\@[\w.\-_]+$/) ||
         ($dirt =~ /a\s+href/)) {
              &error("Illegal characters in $field");
              return $dirt;
      }
      else {
              return $dirt;
      }
  }
  else {
    &error($field);
    return $dirt;
  }
 
}

#####################################################################
### Trim leading and trailing spaces from unrequired fields
#####################################################################

sub clean_unrequired {

  my (@subvars, $dirt, $field) = @_;
  @subvars = @_;

  if ($#subvars > 0) {
    $dirt = $subvars[0];
    $field = $subvars[1];
    $dirt =~ s/^\s+//;
    $dirt =~ s/\s+$//;
  }
  else {
    $field = $subvars[0];
    $dirt = "";
  }
  # print "dirt = $dirt";

  if ($dirt ne "") {
      if (
         ($dirt =~ /Content-Transfer-Encoding/i) ||
         ($dirt =~ /[bcc|cc]:\s+/i) ||
         ($dirt =~ /X-Mailer:/i) ||
         ($dirt =~ /^[\w.\-_]+\@[\w.\-_]+$/) ||
         ($dirt =~ /a\s+href/)) {
              &error("Illegal characters in $field");
              return $dirt;
      }
      else {
              return $dirt;
      }
  }
  else {
    return $dirt;
  }
}

#####################################################################
### Make sure donation amount is in proper format (no ".00")
#####################################################################

sub clean_num {

  my (@subvars, $dirt, $field);
  
  @subvars = @_;
  # print "SUBVARS CLEAN NUM = index 0 = $subvars[0], index 1 = $subvars[1], index eq $#subvars<br />";
  if ($#subvars > 0) {
    $dirt = $subvars[0];
    $field = $subvars[1];
    $dirt =~ s/^\s+//;
    $dirt =~ s/\s+$//;
    $dirt =~ s/\s+//g;
    $dirt =~ s/[\(\)]//g;
    $dirt =~ s/-//g;
    $dirt =~ s/l/1/g;
  }
  else {
    $field = $subvars[0];
    $dirt = "";
  }

  if (($dirt ne "") && ($dirt =~ /^\d*$/)) {
    return $dirt;
  }
  else {
    &error($field);
    return $dirt;
  }
}

#####################################################################
### Make sure email is in proper format
#####################################################################

sub check_email {

  my (@subvars, $dirt, $field);

  @subvars = @_;
  # print "SUBVARS CLEAN NUM = index 0 = $subvars[0], index 1 = $subvars[1], index eq $#subvars<br />";
  if ($#subvars > 0) {
    $dirt = $subvars[0];
    $field = $subvars[1];
    $dirt =~ s/^\s+//;
    $dirt =~ s/\s+$//;
  }
  else {
    $field = $subvars[0];
    $dirt = "";
  }

  if ($dirt =~ /^[\w.\-_]+\@[\w.\-_]+$/) {
    return $dirt;
  }
  else {
    &error($field);
    return $dirt;
  }
}

#####################################################################
### Create array of empty required fields if javascript turned off
#####################################################################

sub error {

  my $field = shift;
  push (@errors, $field);

}

#####################################################################
### log entry
#####################################################################

sub logme {

  my ($i, $date, $referer, $dir, $today, $time, $db, $logfile, @files, $remote_address);

  $date = &get_date;
  ($today, $time) = split (/ /, $date);

  $referer = $q -> referer();
  $remote_address = $q -> remote_addr();

  $dir = "/www/logs/conference";

  eval {
    opendir (DIR, $dir);
    @files = readdir(DIR);
    closedir DIR;
  };
  if ($@) {
    &log_error($@);
    return;
  }
  else {
    $logfile = undef;
    $i = 0;

    for (my $i = 0; $i <= $#files; $i++) {
      if ($files[$i] =~ /forum_log-$today/) {
        $logfile = $dir . "/" . $files[$i];
        last;
      }
      else {
        next;
      }
    }

    unless (defined($logfile)) {
      $logfile = $dir . "/" . "forum_log-$today.txt";
      ($logfile) = $logfile =~ m/(.*)/;
      eval {
        open (LOG, "> $logfile");
        flock (LOG, 2);
        print LOG "$date\t$event\t$prefix\t$lastname\t$firstname\t$affiliation\t$title\t$address\t$address2\t$city\t$state\t$zip\t$country\t$areacode$phone\t$email\t$event_date\t$referer\t$remote_address\n";
        close LOG;
        chmod 0666, $logfile;
      };
      if ($@) {
        &log_error($@);
        &email_admin("logme failed: $@");
        return;
      }
      else {
        return 1;
      }
    }

    else {
      ($logfile) = $logfile =~ m/(.*)/;
      eval {
        open (LOG, ">>$logfile");
        flock (LOG, 2);
        print LOG "$date\t$event\t$prefix\t$lastname\t$firstname\t$affiliation\t$address\t$address2\t$city\t$state\t$zip\t$country\t$areacode$phone\t$email\t$event_date\t$referer\t$remote_address\n";
        close LOG;
      };
      if ($@) {
        &log_error($@);
        &email_admin($@);
        return;
      }
      else {
        return 1;
      }
    }
  }
}

  
#####################################################################
### Get date in YYYY-MM-DD HH:MM format
#####################################################################

sub get_date {

  my ($year, $month, $day, $hour, $minute, $date);

  $year = localtime->year();

  $year = $year - 100;

  if ($year < 10) {
    $year = "200$year";
  }
  else {
    $year = "20$year";
  }

  $month = localtime->mon() + 1;

  if ($month < 10) {
    $month = "0$month";
  }

  $day = localtime->mday();

  if ($day < 10) {
    $day = "0$day";
  }

  $hour = localtime->hour();

  if ($hour < 10) {
    $hour = "0$hour";
  }

  $minute = localtime->min();

  if ($minute < 10) {
    $minute = "0$minute";
  }

  $date = $year . "-" . $month . "-" . $day . " " . $hour . ":" . $minute;

  return ($date);

}

#####################################################################

sub untaint {

  my ($parm) = @_;

  if( $parm =~ /^(.+)$/ ) {
    return $1;
  }
  else {
    return 0;
  }
}

################################################################
##email registration confirmation to user
#################################################################

sub autorespond {

  my ($status, $from_email, $name, $ccto, $msg, $smtp, @to);
 
 $from_email = "events\@cato.org";

  if ($prefix) {
    $name = $prefix . " " . $lastname;
  }
  else {
    $name = $firstname . " " . $lastname;
  }
  $ccto = "";

  $msg = <<"EOF";
From: $from_email
To: $email
Subject: Thank you for registering

Dear $name:
 
Thank you for your interest in Cato Institute activities. This message
confirms that we have received your event registration for:

$clean_event,
to be held $event_date,
at the $location.

EOF

  if ($location =~ /Cato/) {
  $msg .= <<"EOF";
If you have any questions or must cancel your registration, please contact the conference department at events\@cato.org or 202-789-5229.

EOF
  }
  else {
  $msg .= <<"EOF";
Please arrive early, as space is limited. If you have any questions or must cancel your registration, please contact the conference department at events\@cato.org or 202-789-5229.


EOF
  }

$msg .= <<"EOF";

Sincerely,

Conference Team

Cato Institute
1000 Massachusetts Ave NW
Washington, DC 20001

events\@cato.org

EOF

  $status = &send_email($from_email, $msg, $email);
  return $status;

}


#####################################################################
### log errors
#####################################################################

sub log_error {

  my $msg = shift;
  my $date = &get_date;

  my $logfile = '/www/logs/conference/forum_error.log';

  #print "$msg";

  eval {
    open (LOG, ">>$logfile");
    print LOG "$date\t$event\t$prefix\t$lastname\t$firstname\t$zip\t$email\t$areacode-$phone\t$msg\n";
    close LOG;
  };
}


#####################################################################
### send email, log if problems
#####################################################################

sub send_email {
  my ($smtp, $emailfrom, $msg, @to, @bads, @ok);
  ($emailfrom, $msg, @to) = @_;
  #print "passed from and to: $emailfrom ... @to\n";

  #$smtp = Net::SMTP_auth->new('exchange.cato.org', Hello => 'exchange.cato.org');
  #$smtp = Net::SMTP_auth->new('smtp.networkalliance.net', Debug => 1);
  $smtp = Net::SMTP_auth->new('smtp.networkalliance.net');

  if ($smtp) {
    #print "SMTP connection made\n";
    eval {
      $smtp->auth('LOGIN', 'friedrich', 'cato!984');
      #print "Auth ok\n";
      $smtp -> mail($emailfrom);
      #print "emailfrom ok\n";
      @ok = $smtp -> recipient(@to,{SkipBad=>1}) or return $smtp->message;
      #print "ok = @ok\n";
      unless (@ok == @to) {
        @bads = &cmparray(@ok, @to);
        &log_error("can't send mail to: @bads");
      }
      $smtp -> data($msg);
      $smtp -> quit;
      #print "done mailing\n";
    };
    if ($@) {
      #print "we have an error message: $@\n";
      &log_error($@);
      #&email_admin($@);
      return 0;
    }
    else {
      return 1;
      #print "I'm returning 1\n";
    }
  }
  else {
    &log_error("Could not connect to email server, could not send email: $!");
    return 0;
  }
}

#####################################################################
sub cmparray {

  my (@array1, @array2, @union, @intersection, @difference, %count, $element);
  @array1 = shift;
  @array2 = @_;

  @union = @intersection = @difference = ();
  %count = ();

  foreach $element (@array1, @array2) { $count{$element}++;}

  foreach $element (keys %count) {
    push @union, $element;
    push @{ $count{$element} > 1 ? \@intersection : \@difference }, $element;
  }

  return @difference;
}




######################################################################
## Send email to admin if errors in entering data to rand
######################################################################

sub email_admin {

  my ($from, $admin, $msg1);

  $msg1 = shift;
  $admin = "virginia\@cato.org";

  $from = "webserver\@cato.org";
  
  $msg1 = <<"EOF";
From: $from
To: $admin
Subject: Forum Registration ERROR

Error message: $msg1

Applicant info:

Event\t\t=$event

Salutation\t\t=\t$prefix
First name\t\t=\t$firstname
Last name\t\t=\t$lastname
Affiliation\t\t=$affiliation
Title\t\t=$title
Address\t\t=$address
Address2\t\t=$address2
City\t\t=$city
State\t\t=$state
Zip\t\t=$zip
Country\t\t=$country
Phone\t\t=($areacode) $phone
Email\t\t\t=\t$email

EOF

  &send_email($from, $msg1, $admin);
}



#################################################################
## print form with errors
#################################################################

sub print_form {
  
  print <<"EOF";

  <div id="signupform">
  <form method="post" action="https://secure.cato.org/cgi-bin/forum" name="theform" onSubmit="return formCheck(this)">
  <input type="hidden"  id="logo" name="logo" value="$logo">
  <input type="hidden"  id="event" name="event" value="$event">
  <input type="hidden"  id="event_date" name="event_date" value="$event_date">
  <input type="hidden"  id="location" name="location" value="$location">
  <input type="hidden"  id="emailto" name="emailto" value="$emailto">
  <input type="hidden"  id="confURL" name="confURL" value="$confURL">

<table border="0" cellspacing="0" cellpadding="5" width="425">
  <tr> 
    <td align="center" colspan="2"> (Required fields are marked with <font color="#FF0000">*</font>.)
      <img src="shim.gif" height="1" width="95"><br /></td>
  </tr>
  <tr>
    <td width="115" align="right" valign="bottom">
      <font color="red">*</font>&nbsp;Salutation </td>
    <td align="left" valign="bottom">
      <select name="prefix">
        <option value=""></option>
EOF

  my @prefixes = qw(Mr. Ms. Mrs. Dr.);
  unless (defined($prefix)) {
    $prefix = "";
    print "PREFIX = $prefix<br />";
  }
  foreach (@prefixes) {
    if ($_ eq $prefix) {
      print "<option value=\"$_\" selected>$_</option>";
    }
    else {
      print "<option value=\"$_\">$_</option>";
    }
  }
  print <<"EOF";

    </td>
  </tr>
  <tr>
    <td align="right" valign="bottom"><font color="#FF0000">*</font>&nbsp;First  Name </td>
    <td align="left" valign="bottom" width="290"><img src="shim.gif" height="1" width="290"><input type="text" size="12" maxlength="256" name="firstname" id="firstname" value="$firstname"></td>
  </tr>
  <tr>
    <td align="right" valign="bottom"><font color="#FF0000">*</font>&nbsp;Last Name</td>
    <td align="left" valign="bottom"><input type="text" size="12" maxlength="256" name="lastname" id="lastname" value="$lastname"></td>
  </tr>
  <tr>
    <td align="right" valign="bottom"><font color="#FF0000">*</font>&nbsp;Affiliation</td>
    <td align="left" valign="bottom"><input type="text" size="15" maxlength="256" name="affiliation" id="affiliation" value="$affiliation"></td>
  </tr>
  <tr>
    <td align="right" valign="bottom">&nbsp;Title</td>
    <td align="left" valign="bottom"><input type="text" size="15" maxlength="256" name="title" id="title" value="$title"></td>
  </tr>
EOF

  if ($event =~ /Intern Alumni Reunion/){
	print "<tr><td width=\"159\" align=\"right\"><font color=\"#FF0000\">*</font>Semester Interned </td>
<td>
                <select name=\"session\">
                    <option value=\"\"></option>
                    <option value=\"spring\""; if($semester =~ /spring/) {print "selected"};print">Spring</option>

                    <option value=\"summer\""; if($semester =~ /summer/) {print "selected"};print">Summer</option>
                    <option value=\"fall\""; if($semester =~ /fall/) {print "selected"};print">Fall</option>
                    <option value=\"not_sure\""; if($semester =~ /not sure/) {print "selected"};print">Not Sure</option>
 </select>            </td>
        </tr>

        <td width=\"159\" align=\"right\"><font color=\"#FF0000\">*</font>Year Interned </td>

          <td><input type=\"text\" size=\"5\" maxlength=\"11\" name=\"iyear\" value=\"$iyear\"></td>
        </tr>
";
}
  print <<"EOF";
  
  <tr>
  
  <td align="right" valign="bottom"><font color="#FF0000">*</font>&nbsp;Address</td>
    <td align="left" valign="bottom"><input type="text" size="32" maxlength="256" name="address" id="address" value="$address"></td>
  </tr>
  <tr>
    <td align="right" valign="bottom"><font color="#FF0000"></font>&nbsp;Address 2</td>
    <td align="left" valign="bottom"><input type="text" size="32" maxlength="256" name="address2" id="address2" value="$address2"></td>
  </tr>
  <tr>
    <td align="right" valign="bottom"><font color="#FF0000">*</font>&nbsp;City</td>
    <td valign="bottom"><input type="text" size="16" maxlength="256" name="city" id="city" value="$city"></td>
  </tr>
  <tr>
    <td align="right" valign="bottom"><font color="#FF0000">*</font>&nbsp;State</td>
    <td valign="bottom"><input type="text" size="2" maxlength="2" name="state"  id="state" value="$state"> <span style="color: #FF0000">*</span>&nbsp;Zip <input type="text" size="10" maxlength="10" name="zip" id="zip" value="$zip"></td>
  </tr>
  <tr>
    <td align="right" valign="bottom"><font color="#FF0000">*</font>&nbsp;Country</td>
    <td valign="bottom"><input type="text" size="19" maxlength="256" name="country" id="country" value="$country"></td>
  </tr>
  <tr>
    <td align="right" valign="bottom"><font color="#FF0000">*</font>&nbsp;Area Code</td>
    <td align="left" valign="bottom"><input type="text" size="5" maxlength="10" name="areacode"  id="areacode" value="$areacode"> <font color="#FF0000">*</font>&nbsp;Phone <input type="text" size="10" maxlength="20" name="phone"  id="phone" value="$phone"></td>
  </tr>
  <tr>
    <td align="right" valign="bottom"><font color="#FF0000">*</font>&nbsp;Email </td>
    <td align="left" valign="bottom"><input type="text" size="32" maxlength="256" name="email" id="email" value="$email"></td>
  </tr>
EOF

  if ((defined($special_invite)) && ($special_invite eq "yes")) {
    print <<"EOF";
     </tr>
  <tr>  <td align="right" valign="top"><input type="checkbox" name="special-invite" value="I received a special invitation and am an out of town guest." /></td>
   <td align="left" valign="top">I received a special invitation and am an out of town guest.</td>
    
  </tr>
EOF
  }
  print <<"EOF";
  <tr>
    <td align=center colspan="2"><input type="submit" value="Submit" name="submit" id="submitbutton"> </td>
  </tr>
</table>

</form>
</div>

EOF
}

#################################################################
## enter registration data in forum_reg table in cato database
#################################################################

sub insert {

  my ($today, $time, $database, $hostname, $dsn, $username, $password, $dbh, $eventid, $sth, $i, %attributes, $error_log, $error_msg);
 
  ($today, $time) = split (/ /, &get_date);

  if ($confURL =~ /event\.php\?eventid=(\d+)/) {
    $eventid = $1;
  }
  else {
    if ($event =~ /Politics of Freedom/) {
      $eventid = "4199";
    }
  }

  $error_log = "/www/logs/conference/forum_error.log";

  $database = "cato"; 
  $hostname = "localhost";
  $dsn = "DBI:mysql:database=$database;host=$hostname"; 
  $username = "forum";
  $password = "f04um4E8";

  ## disable automatic error checking while trying to connect to db so we don't die or warn
  %attributes = (PrintError => 0, RaiseError => 0);

  $i = 0;

  $dbh = DBI -> connect( $dsn, $username, $password, \%attributes);
  if ($dbh) {
    eval {
      # print "DBH connected";

      ## Enable auto error checking on dbhandle so we can catch errors in eval
      $dbh->{PrintError} = 1;
      $dbh->{RaiseError} = 1;

      ## put read lock on table
      # $dbh -> do("LOCK TABLES interns READ LOCAL");
      #
      ## Insert row
      $dbh -> do("INSERT INTO forum_reg ( forum_reg_eventid, forum_reg_date, forum_reg_prefix, forum_reg_fname, forum_reg_lname, forum_reg_affiliation, forum_reg_title, forum_reg_address, forum_reg_address2, forum_reg_city, forum_reg_state, forum_reg_zip, forum_reg_country, forum_reg_phone, forum_reg_email )
                  VALUES ( \"$eventid\", \"$today\", \"$prefix\", \"$firstname\", \"$lastname\",  \"$affiliation\", \"$title\", \"$address\", \"$address2\", \"$city\", \"$state\", \"$zip\", \"$country\", \"$areacode-$phone\", \"$email\" )" );


      # $dbh -> do("UNLOCK TABLES");

      $dbh->disconnect; 
    };

    if ($@) {
      $error_msg = $@;
      &log_error($error_msg);
      &email_admin($error_msg);
      return;
    }
    else {
      return 1;
    }
  }

  else {
    $error_msg = $DBI::errstr;
    #print "DID NOT CONNECT -- $error_msg";
    my $logged = &log_error($error_msg);
    #print "LOGGED ERROR + $logged";
    &email_admin($error_msg);
    return;
  } 

}

