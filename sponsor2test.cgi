#!/usr/bin/perl -Tw 
# 
# script to process sponsorship enrollments


use v5.14;
use strict;
#use diagnostics;
#use utf8;
use feature qw(switch);
use Net::SMTP;
use Net::SMTP_auth;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use XML::LibXML;
use MoXAddPerson;
use MoXAddGift;
#use Snoopy;
use Business::CCCheck qw(CC_digits CC_expired);
use DBD::SQLite;
use XML::Entities;
use HTML::Entities;

$|++;

my ($page, $q, $donate_method, $emailto, $GLaccount, $donation_amount, $sponsor_level, $prefix, $firstname, $lastname, $suffix, $addrtype, $street, $city, $state, $zip, $country, $phone, $company, $position, $creditcard, $accountno, $expdate_mon, $expdate_yr, $expdate, $cardname, $cvv, $recurring, $email, $comments, $accountno_msg, $chkdate_msg, $personID, $renewal, @errors, @errors_modules, $auth_code, $error_msg, $logme_status, $badip, $ban_message_user, $error_message_admin, $snoopy, $AddPerson, $AddPersonResult, $AddPersonParseXMLResult, $AddGift, $AddGiftResult, $AddGiftParseXMLResult, $authcode); 


#####################################################################
###  Main
#####################################################################

#check that form info has been passed using SSL
#$ENV{HTTPS} eq "on" or &error("This is not a secure transaction.");

#get rid of any crap someone tries to send in a query string without killing the program
if (length($ENV{QUERY_STRING}) > 0) {
  $ENV{QUERY_STRING} = "";
}

#have all xml entities at hand to encode user input
%HTML::Entities::char2entity = %{
    XML::Entities::Data::char2entity('all');
}; 

$q = CGI -> new;
 
print $q -> header();


#print "header printed...\n";

#&start_thankyou;

&get_vars;

unless (@errors) { ## if there are errors in vars, don't bother to snoop or process cc
  #print "passed errors test, trying to run snoopy...\n";
  #$snoopy = Snoopy -> new (badip => $badip);
  #($ban_message_user, $error_message_admin) = $snoopy -> snoop;
  #print "ban message = $ban_message_user error message = $error_message_admin";

  #if ($ban_message_user) { #this guy is banned, let's bail
  #  print "did not pass snoopy...\n";
  #  &error_modules($ban_message_user);
  #  &log_error("error ban_message_user: $ban_message_user");
  #  &email_admin($error_message_admin);
  #}
  #else { #ok, no reports from snoopy, let's process this donation
    #print "running addperson...n";
    $AddPerson = MoXAddPerson -> new ( #get a personID from MO

                        prefix          =>      $prefix,
                        lastname        =>      $lastname,
                        firstname       =>      $firstname,
                        suffix          =>      $suffix,
                        addrtype        =>      $addrtype,
                        street          =>      $street,
                        city            =>      $city,
                        state           =>      $state,
                        zip             =>      $zip,
                        country         =>      $country,
                        phone           =>      $phone,
                        email           =>      $email,
                        company         =>      $company,
                        position        =>      $position

    );
    #print "Adding person...<br />\n";
    $AddPersonResult = $AddPerson -> MoXAddPerson;
  
    $AddPersonParseXMLResult = &parseXML("AddPerson", $AddPersonResult);

    unless ($AddPersonParseXMLResult =~ /^xout=/) { # AddPerson failed
      #print "addperson failed, check logs\n";
      &error_modules($AddPersonParseXMLResult);
      &log_error("error in AddPerson call: $AddPersonParseXMLResult");
      #&email_admin($AddPersonParseXMLResult);
    }
    else { #AddPerson returned a personID so we can continue to add the donation
      #print "addperson passed, running add gift\n";
      if ($AddPersonParseXMLResult =~ /^xout=(.*)/) {
        $personID = $1;
        #print "personID = $personID\n";

        #now we have a personID to give to AddGift and cc info is scrubbed:
        $AddGift = MoXAddGift -> new (
 
                        personID       =>      $personID,
                        recurring      =>      $recurring,
                        ccnumber       =>      $accountno,
                        cardname       =>      $cardname,
                        expdate        =>      $expdate,
                        cvv            =>      $cvv,
                        totalamount    =>      $donation_amount,
                        email          =>      $email,
                        dedication     =>      $comments
        );
        $AddGiftResult = $AddGift -> MoXAddGift;
        $AddGiftParseXMLResult = &parseXML("AddGift", $AddGiftResult);
        unless ($AddGiftParseXMLResult =~ /^xout=/) { # AddGift failed
          #print "addgift failed, check logs\n";
          &error_modules($AddGiftParseXMLResult);
          &log_error("error in AddGift call: $AddGiftParseXMLResult");
          #&email_admin("error in AddGift call: $AddGiftParseXMLResult");
        }
        else { #it's all good, everything worked, log and notify
          if ($AddGiftParseXMLResult =~ /^xout=(.*)/) { 
            $authcode = $1;
            #print "AddGiftParseXMLResult = $AddGiftParseXMLResult ";
            &logme;
            #&emailme; #MO
            #&autorespond; #MO
          }
        }
      }
    }
  #} snoopy else 
}

&thankyou; #will print errors from get_vars or modules or confirmation

exit;

#####################################################################
### html headers
#####################################################################

sub start_thankyou {

  print <<"EOF";

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">
<html><head></head>
<body> 


EOF
}




#####################################################################
### Get form name/value pairs
#####################################################################

sub get_vars {

  my ($proc, $cc_expired_result, $cc_digits_result);

  #print "made it to sub get_vars\n";
  $badip = $q -> remote_addr(); 
  $donate_method = $q -> param('donate-method');
  $emailto = $q -> param('emailto');
  $renewal = $q -> param('os0'); #New, Renewal, Bonus
  $GLaccount = "4110";
    unless ($renewal) {
      &error("Donation type (new, renewal, bonus contribution)");
    }

  $donation_amount = &clean_num($q -> param('amount'), "Contribution Amount");
  if ($donate_method eq "credit-card") {
#  old switch statement for noting sponsor level on autoresponse -- no longer needed b/c MO notifies
#  keep in case we want to use it in the future

#  for (($renewal eq "yes") || ($renewal eq "no")) { #anything other than a bonus contribution
#      when (($donation_amount >= 0) && ($donation_amount <= 49)) { $sponsor_level = "" };
#      when (($donation_amount >= 50) && ($donation_amount <= 99) && ($renewal eq "no")) { $sponsor_level = "Introductory" };
#      when (($donation_amount >= 50) && ($donation_amount <= 99) && ($renewal eq "yes")) { $sponsor_level = ""; };
#      when (($donation_amount >= 100) && ($donation_amount <= 499)) { $sponsor_level = "Basic" };
#      when (($donation_amount >= 500) && ($donation_amount <= 999)) { $sponsor_level = "Sustaining" };
#      when (($donation_amount >= 1000) && ($donation_amount <= 4999)) { $sponsor_level = "Patron" };
#      when (($donation_amount >= 5000) && ($donation_amount <= 24999)) { $sponsor_level = "Benefactor" };
#      when (($donation_amount >= 25000)) { $sponsor_level = "Cato Club 200" };
#      default { $sponsor_level = "" }
#  }
#  #print "sponsor_level = $sponsor_level<br />";

  #$creditcard = &clean($q -> param('creditcard'),"Credit card type (Visa, MasterCard, American Express, Discover)");
  $accountno = &clean_num($q -> param('accountno'), "Credit Card Account Number");
  $expdate_mon = $q -> param('expdate_mon');
  $expdate_yr  = substr($q -> param('expdate_yr'), -2);
  $expdate = $expdate_yr . $expdate_mon;
  $cardname = &clean($q -> param('cardname'), "Name on card");
  $cvv = &clean($q -> param('cvv'), "CVV");
  $recurring = $q -> param('recurring-donation');
  unless ($recurring eq "true") {
    $recurring = "false";
  }
  #$cc_digits_result = CC_digits($accountno);
  #unless ($cc_digits_result) { #result is true, so if there is one, it's good
  #  &error("Credit card is not a valid number");
  #  &log_error("credit card is not a valid number ");
  #}

  #$cc_expired_result = CC_expired($expdate_mon, "20" . $expdate_yr);
  #if ($cc_expired_result) { #result is true, so if there is one, it's good
  #  &error("Expiration Date: expired or bad format")
  #}
  $prefix = &clean_unrequired($q -> param('prefix'), ""); 
  $firstname = &clean($q -> param('firstname'), "First Name");
  $lastname = &clean($q -> param('lastname'), "Last Name");
  $suffix = &clean_unrequired($q -> param('suffix'), "");
  $addrtype = &clean($q -> param('addr-type'), "Address Type");

  if ($addrtype == 1) { #business address
    $company = &clean_unrequired($q -> param('company'), "");
    $position = &clean_unrequired($q -> param('position'), "");
  }
  else { #addrtype is 2 or something else, count as home
    $company = &clean_unrequired($q -> param('affiliation'), "");
  }
  $country = &clean($q -> param('country'), "Country");
  $state = &clean_unrequired($q -> param('state'), "");
  $street = &clean($q -> param('address'), "Street");
  #print "street = $street<br />\n";
  $city = &clean($q -> param('city'), "City");
  $zip = &clean($q -> param('zip'), "Zip/Postal Code");
  $phone = &clean_num($q -> param('phone'), "Phone Number");
  $email = &check_email($q -> param('email'), "Email Address");
  $comments = &clean_unrequired($q -> param('comments'), "");
  $comments = '<![CDATA[' . $comments . ']]>';
  #print "made it to the end of get_vars\n";
  }
  elsif ($donate_method eq "paypal") {
    &paypal;
  }
 
} 

#####################################################################
#SUBROUTINES
#####################################################################

#####################################################################
### Results page after submit
#####################################################################

sub thankyou {
  #check post data
  #my @params = $q -> param;
  #foreach my $name (@params) {
  #  print "$name=$q -> param{$name}<br>";
  #}
  my (@errors_html);

  if ((@errors_modules) || (@errors)) { 
    if (@errors_modules) { #errors from MO or bank
      print <<"EOF";
{
  "status": "bad",
  "feedback": "<p>We're sorry, your transaction could not be completed: <font color='red'><strong>@errors_modules</strong></font></p>"
}
EOF
    }
    else { #errors in user input
      for (@errors) {
        push (@errors_html,  "<li> $_</li>");
      }
      print <<"EOF";
{
  "status": "bad",
  "feedback": "<p><font color='red'><strong>Please check the following field(s) and make sure the information entered is correct:</strong></font></p><ul>@errors_html</ul></p>"
}
EOF
    }
  } #end if errors
  else {
    if ($recurring eq "true") {
      if ($donation_amount > 50) {
        print <<"EOF";
{
    "status": "good",
    "feedback": "<H2>Thank you for supporting the Cato Institute.</H2><p>The Cato Institute has received your recurring monthly donation of \$$donation_amount. An acknowledgement will be mailed to you confirming your contribution. Thank you very much for your generosity.</p>",
    "authcode": "$authcode"
}
EOF
      }
      else {
        print <<"EOF";
{
    "status": "good",
    "feedback": "<H2>Thank you for supporting the Cato Institute.</H2><p>The Cato Institute has received your recurring monthly donation of \$$donation_amount. Thank you very much for your generosity.</p>",
    "authcode": "$authcode"
}
EOF
      }
    }
    else {
      if ($donation_amount > 50) {
        print <<"EOF";
{
    "status": "good",
    "feedback": "<H2>Thank you for supporting the Cato Institute.</H2><p>The Cato Institute has received your donation of \$$donation_amount. An acknowledgement will be mailed to you confirming your contribution. Thank you very much for your generosity.</p>",
    "authcode": "$authcode"
}
EOF
      }
      else {
        print <<"EOF";
{
    "status": "good",
    "feedback": "<H2>Thank you for supporting the Cato Institute.</H2><p>The Cato Institute has received your donation of \$$donation_amount. Thank you very much for your generosity.</p>",
    "authcode": "$authcode"
}
EOF



      }
    }
  }
}

#####################################################################
### log entry
#####################################################################

sub logme {

  my ($database, $dbh, $sth);
 
  #print "in logme<br />\n";
  $database = "/www/securessl/cgi-bin/scripts/db/logme.db";

  eval {
    $dbh = DBI->connect("dbi:SQLite:dbname=$database", "", "", { RaiseError => 1 }) or die "$DBI::errstr";
    if ($dbh) {
      $dbh->trace(2);
      #print "Connected to DB logme ok<br />\n";
      my $sth = $dbh->prepare("INSERT INTO donate ( 
			personID,
			prefix ,
                	lastname,
			firstname, 
			addrtype,
			company,
			street,
			city,
			state,
			zip,
			country,
			phone,
			email,
			recurring,
			donation_amount,
			GLaccount,
			auth_code,
			remote_address
		) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
      $sth->execute(
			$personID,
			$prefix,
                	$lastname,
			$firstname,
			$addrtype,
			$company,
			$street,
			$city,
			$state,
			$zip,
			$country,
			$phone,
			$email,
			$recurring,
			$donation_amount,
			$GLaccount,
			$auth_code,
			$badip ) or die $DBI::errstr;
    }
  };
  if ($@) {
    #print "error accessing logme/donate database/table = $@<br />\n";
    &log_error("error logme 1: $@");
    #&email_admin($@);
    #return;
  }
  undef $dbh;
}

#####################################################################
### log errors
#####################################################################

sub log_error {
  my ($msg, $database, $dbh);
  $msg = shift;
  $database = "/www/securessl/cgi-bin/scripts/db/logme.db";

  eval {
    my $dbh = DBI->connect("dbi:SQLite:dbname=$database", "", "", { RaiseError => 1 }) or die "$DBI::errstr";
    if ($dbh) {
      #print "Connected to DB logme ok<br />\n";
      ## Insert IP into table and auto timestamp.
      $dbh -> do("INSERT INTO errors ( 
			prefix ,
                	lastname,
			firstname, 
			zip,
			phone,
			email,
			remote_address,
			msg
		) VALUES (
			'$prefix',
                	'$lastname',
			'$firstname',
			'$zip',
			'$phone',
			'$email',
			'$badip',
                        '$msg'
		)") or die $DBI::errstr;
    }
  };
  if ($@) {
    &log_error("error logme 2: $@");
    &email_admin($@);
    return;
  }
  undef $dbh;
}
  

#####################################################################
### scrub user input for unrequired field
#####################################################################

sub clean_unrequired {

  my (@subvars, $dirt, $field);
  @subvars = @_;

  $dirt = $subvars[0];
  $dirt =~ s/^\s+//;
  $dirt =~ s/\s+$//;
  $dirt =~ s/<//g;
  $dirt =~ s/>//g;
  $dirt = encode_entities($dirt); #encode any remaining chars for XML

  if ($dirt ne "") {
      if (
         ($dirt =~ /Content-Transfer-Encoding:/i) ||
         ($dirt =~ /[bcc|cc]:\s+/i) ||
         ($dirt =~ /X-Mailer:/i) ||
         ($dirt =~ /a\s+href/)) {
              &error("Illegal characters $dirt");
              $dirt = "";
              return $dirt;
      }
      else {
              return $dirt;
      }
  }
  else {
    return $dirt;
  }
}

#####################################################################
### scrub required user input; return error if usr input is null
#####################################################################

sub clean {

  my (@subvars, $dirt, $field);
  @subvars = @_;
  # print "SUBVARS CLEAN = index 0 = $subvars[0], index 1 = $subvars[1], index eq $#subvars<br />";

  if ($#subvars > 0) {
    $dirt = $subvars[0];
    $field = $subvars[1];
    $dirt =~ s/^\s+//;
    $dirt =~ s/\s+$//;
    $dirt =~ s/<//g;
    $dirt =~ s/>//g;
    $dirt = encode_entities($dirt); #encode any remaining chars for XML
  }
  else {
    $field = $subvars[0];
    $dirt = "";
  }
  # print "dirt = $dirt";

  if ($dirt ne "") {
      if (
         ($dirt =~ /Content-Transfer-Encoding:/i) ||
         ($dirt =~ /[bcc|cc]:\s+/i) ||
         ($dirt =~ /X-Mailer:/i) ||
         ($dirt =~ /a\s+href/)) {
              &error("Illegal characters in $field field");
              return $dirt;
      }
      elsif ($dirt =~ /^Select/i) {
         $dirt = "";
         return $dirt;
      }
      else {
         return $dirt;
      }
  }
  else {
    &error($field);
    return $dirt;
  }
}

#####################################################################
### Make sure donation amount is in proper format (no ".00")
#####################################################################

sub clean_num {

  my (@subvars, $dirt, $field);

  @subvars = @_;
  # print "SUBVARS CLEAN NUM = index 0 = $subvars[0], index 1 = $subvars[1], index eq $#subvars<br />";
  if ($#subvars > 0) {
    $dirt = $subvars[0];
    $field = $subvars[1];
    $dirt =~ s/^\s+//;
    $dirt =~ s/\s+$//;
    $dirt =~ s/\s+//g;
    $dirt =~ s/[\(\)]//g;
    $dirt =~ s/-//g;
    $dirt =~ s/l/1/g;
    $dirt =~ s/\.00//;
  }
  else {
    $field = $subvars[0];
    $dirt = "";
  }

  if (($dirt ne "") && ($dirt =~ /^\d*$/)) {
    return $dirt;
  }
  else {
    &error($field);
    return $dirt;
  }
}


#####################################################################
### Make sure email is in proper format
#####################################################################

sub check_email {

  my (@subvars, $dirt, $field);

  @subvars = @_;

  if ($#subvars > 0) {
    $dirt = $subvars[0];
    $field = $subvars[1];
    $dirt =~ s/^\s+//;
    $dirt =~ s/\s+$//;
  }
  else {
    $field = $subvars[0];
    $dirt = "";
  }
  if ($dirt =~ /^[\w.\-_]+\@[\w.\-_]+$/) {
    return $dirt;
  }
  else {
    &error($field);
    return($dirt);
  }
}

#####################################################################
### Create array of required fields errors
#####################################################################

sub error {

  my $field = shift;
  # print "sub error field = $field";
  push (@errors, $field);

}

#####################################################################
### Create array of module errors
#####################################################################

sub error_modules {

  my $err = shift;
  # print "sub error_modules = $err";
  push (@errors_modules, $err);

}

#####################################################################
### Replace all but last digits of credit card number with Xs
#####################################################################

sub xaccount {

  my (@acct, $n, $i, $account);
  @acct = (split//, $accountno);

  if ($creditcard eq "amex") {
    $n = 4;
  }
  else {
    $n = 3;
  }

  for ($i = 0; $i < ($#acct - $n); $i++) {
    $acct[$i] =~ s/\d/X/;
  }

  $account = join("", @acct);

  return $account;
}


########################################################################
## Parse MO API output
########################################################################

sub parseXML {

  my ($caller, $resp, $parser, $doc, $node, $fault, $return, $xout, $error, $PersonID, $ourreturn);

  ($caller, $resp) = @_;
  
  $resp = $resp -> as_string;
  #print "XML == $resp\n\n";

  $resp =~ s/^.*?</</s; #remove HTTP response headers

  $parser = new XML::LibXML();

  $doc = $parser->load_xml(string => $resp);
  #print "doc = $doc\n";

  for $node ($doc -> findnodes('//*')) {
    if ($node -> nodeName =~ /soap:Fault/) {
        $fault = "$node";
        #print "fault = $fault\n";
    }
    else {
        if ($node -> nodeName =~ /^return/) {
            $return = $node -> textContent;
            #print "return = $return\n";
        }
        if ($node -> nodeName =~ /^xOut/) {
            $xout = $node -> textContent;
            if ($caller eq "AddGift") {
                if (($xout =~ /<\s*AuthCode\s*>(\d*)<\/Auth/) || ($xout =~ /&#60;AuthCode&#62;(\d*)&#60;\/Auth/)) {
                    $xout = $1;
                }
                else {
                    $xout = "123";
                }
            }
            else {
              return "xout=$xout";
            }
        #print "xout = $xout\n";
        }
        if ($node -> nodeName =~ /^ErrorMsg/) {
            $error = $node -> textContent;
            #print "error = $error\n";
        }
    }
  }

  if ($fault) {
    return "fault=$fault";
  }
  else {
    if (($return eq "true") && ($xout)) {
      #print "return = xout $xout\n";
      $ourreturn = "xout=$xout";
    }
    elsif (($return eq "false") || ($return eq "error")) {
      #print "return = error $error\n";
      $ourreturn = "error=$error";
    }
    else {
      $ourreturn = "";
    }
  }
  #print "OURRETURN = $ourreturn\n";
  return $ourreturn;
}


###########################################################################
###paypal transaction
###########################################################################

#sub paypal {
  #test credentials
    #Username: testpaypal1_api1.cato.org
    #Password: 1388503871
    #Signature: An5ns1Kso7MWUdW4ErQKJJJ4qi4-AqpFK4HaTw6aIFqRPNrm95MbGxfZ


#}


