#!/usr/bin/perl -w

use strict;
use warnings;
use utf8;
use diagnostics;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Business::PayPal::NVP;
use DBD::SQLite;

$|++;
my ($q, $caller, $req_meth, $nvp, $amount, $invnum, %resp, $token, $payerid, $database, $dbh);
$q = new CGI;
$caller = $q -> referer();
$req_meth = $q -> request_method();

if ($req_meth eq "GET") { 
  if ($q -> param('status') eq "success") {
    if (defined($q->param('PayerID'))) { #means we've already called get, now we can do
      #$payerid = $q->param('PayerID');

      %resp = $nvp->DoExpressCheckoutPayment( TOKEN         => $token,
                                       AMT           => $amount,
                                       PAYERID       => $payerid,
                                       PAYMENTACTION => 'Sale' );
       print $q->header();
       print <<"EOF";
         <body><h1>thanks!</h1></body></html>
EOF
    }
    else { #no payerid, so we need to call get with the token from paypal redirect URL, print submit page to user
      $token = $q->param('token');
      print "Content-type=text/html\n\n";
      print <<EOF;
      <body><form action="https://securessl.cato.org/cgi-bin/scripts/paypal.cgi" method="POST"><input type="hidden" name="call" value="do"><input type="hidden" name="token" value="$token"><input type="submit"></form></body></html>
EOF
    }
  }
  else { #status was bad
    print "$q -> param('status') is bad<br />\n";
  }
}
else { #method was post
  if (defined($q->param('call'))) { #this is the second submit
      $token = $q->param('token');

      #%resp = $nvp->GetExpressCheckoutDetails( TOKEN => $token );
      $nvp->GetExpressCheckoutDetails( TOKEN => $token );
      #$payerid = $resp{PayerID};
  }
  else {
#$amount = $q->param("donation");
$amount = 3;
$nvp = new Business::PayPal::NVP( test => { user => 'testpaypal1_api1.cato.org',
                                            pwd  => '1388503871',
                                            sig  => 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-AqpFK4HaTw6aIFqRPNrm95MbGxfZ' },
                                  live => { user => 'foo.domain.tld',
                                            pwd  => '55553333234',
                                            sig  => 'Afk4js43K.kKdkwj.i9w39fswjeifji-2oj3k' },
                                branch => 'test' );
   
$invnum = time;
%resp = $nvp->SetExpressCheckout( AMT           => $amount,
                                  CURRENCYCODE  => 'USD',
                                  DESC          => 'donation',
                                  CUSTOM        => 'thank you for your money!',
                                  INVNUM        => $invnum,
                                  PAYMENTACTION => 'Sale',
                                  RETURNURL     => 'https://securessl.cato.org/cgi-bin/scripts/paypal2.cgi?status=success',
                                  CANCELURL     => 'https://securessl.cato.org/cgi-bin/scripts/paypal.cgi?status=bad', );
 
$token = $resp{TOKEN};

#insert into db here

$database = "/www/securessl/cgi-bin/scripts/db/paypal.db";
$dbh = DBI->connect("dbi:SQLite:dbname=$database", "", "",  { RaiseError => 1 }) or die $DBI::errstr;
$dbh->do("insert into pp ( token, donation_amount ) values ( \"$token\", \"$amount\" )") or die $DBI::errstr;
$dbh->disconnect;

#redirect user to paypal.com

print $q->redirect("https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=$token");
  }
}

#foreach my $key (keys %resp) {
#  print "SetExpressCheckout key = $key, value = $resp{$key}\n";
#}
    
#https://securessl.cato.org/cgi-bin/scripts/paypal.cgi?status=sucess&token=EC-31F75396L9365184A&PayerID=FZZX2RVUZBF9G

